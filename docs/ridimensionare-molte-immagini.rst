***************************************************************************
Ridimensionare con una sola operazione molte immagini
***************************************************************************

Premessa
===============

Il file manager utilizzato di default (Thunar) non permette di ridimensionare con una sola operazione più immagini. Se c'è bisogno di fare questa operazione, la soluzione più veloce è quella di modificare il file manager utilizzato dal sistema.
Nella distribuzione è installato un altro file manager, il cui nome è Nautilus, il quale permette invece di eseguire facilmente questa operazione.
La scelta di modificare il file manager dipende dall'utente. In questo capitolo spieghimo come fare

Modificare il file manager (Gestore di file)
============================================

.. figure:: images/ridim-imm/whisker.png
    :width: 40px
    :align: right
    
* Cliccare sull'icona delle Applicazioni (Menu Whisker, in genere in basso a sinistra)

* Apertosi il menu delle Applicazioni, selezionare nell'elenco a destra Impostazioni, (come nella figura qui sotto):

.. figure:: images/ridim-imm/impostazioni.png
    :width: 300px
    :align: center
	    
* Nell'elenco che si apre a sinistra selezionare ``Applicazioni preferite``, come nell'immagine sottostante:

.. figure:: images/ridim-imm/preferite.png
    :width: 300px
    :align: center

* Si apre una nuova finestra come nell'immagine sottostante, nella quale è possibile configurare varie opzioni. Quella che interessa a noi si trova selezionando  la scheda Utilità (nella parte alta della scheda) :

.. figure:: images/ridim-imm/scelta.png
    :width: 300px
    :align: center

* A questo punto, cliccando sul menu a tendina ``Gestore dei file``, si può selezionare Nautilus anzichè il predefinito Thunar.

* Chiudendo eventuali istanze aperte del Gestore di File  e riaprendo lo stesso, è possibile eseguire l'operazione immediarta di ridimensionamento di più immagini

Ridimensionare più immagini
====================================

* Accedere con il Festore File alla cartella in cui ci sono le immagini da modificare
* Selezionare tutte le immagini che si vogliono ridimensionare e e cliccare col tasto dx del mouse all'interno di una di quelle selezionate: si apre un menu contestuale come nella figura sottostante:

.. figure:: images/ridim-imm/operaz01.png
    :width: 300px
    :align: center

* Cliccare su ``Ridimensionare le immagini``: si apre una finestra che contiene le operazioni per il ridimensionamento:

.. figure:: images/ridim-imm/operaz-02.png
    :width: 300px
    :align: center
    
* Selezionare ``Scale`` e inserire il valore percentuale di cui si vuol ridurre l'immagine
    
* Digitare (o selezionare quello presente ".resized) una estensione che viene aggiunta ai nomi dei file: è possibile in alternativa selezionare l'opzione ``Resize in place`` , ma in quersto modo si perdono le immagini originali. 
    
* Cliccando sul pulsante ``Resize`` l'operazione viene eseguita.
    
    
    


