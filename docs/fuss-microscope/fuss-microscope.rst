Utilizzo di fuss-microscope per microscopi intel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Premessa
====================

 È un'applicazione FUSS per lavorare con il microscopio ``Intel Play QX3`` 
 ed osservare oggetti a vari ingrandimenti nel monitor del computer collegato.
 Sfrutta l'applicativo di visualizzazione gucview e consente di fare fotografie e video.

Utilizzo
============

1) Lanciare l’applicazione da **Menu** dopo aver collegato il microscopio via **USB**.


2) Cliccare su **GUCVIEW**.

.. image:: images/menu.png
   :align: center
   :scale: 50%

|   
    
Nel Menu delle Impostazioni **Video Controls** di gucview 

3) Selezionare il dispositivo corretto (``Intel Play QX3``) in  **Dispositivi**.

4) Aumentare al massimo la risoluzione in **Risoluzione** ed eventualmente selezionare o regolare altri valori.

.. image:: images/gucview-video-controls.png
   :align: center
   :scale: 50%  

|
     
5) A questo punto si possono accendere le due lampade sia dall’interfaccia di gucview (**Illuminator 1 e 2**)
che da quella principale di fuss-microscope (**ON OFF**); in questo secondo caso potrebbe essere talvolta necessario cliccare prima su OFF e poi su ON.

.. image:: images/gucview-controllo-immagini.png
   :align: center
   :scale: 50%

