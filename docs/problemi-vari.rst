***********************************
Problemi e soluzioni
***********************************

Problema di accesso al browser
==============================================

Compare la seguente finestra:

.. figure:: images/err-1.png
    :width: 300px
    :align: center

Il problema è dovuto al fatto che, quando si lancia firefox, si crea un file (.parentlock) che non permette l’accesso da un altro PC della rete con lo stesso utente . Ciò può essere dovuto  a una chiusura precedente non corretta del browser o a problemi momentanei del PC su cui si è lavorato precedentemente.

* Visualizzare i file nascosti

* Doppio click  sulla cartella .mozilla , quindi sulla cartella firefox

* All’interno della cartella firefox si trova una cartella dal nome casuale.default, dove casuale è una sequenza casuale di caratteri

* Doppio click su questa cartella, quindi cercare il file .parentlock ed eliminarlo.

Problema di visualizzazione dei pannelli sul desktop
========================================================================

Se non si vede correttamente il desktop in tutte le sue parti, è possibile fare questa operazione, che riporta alla situazione originale il desktop

* aprire un terminale (vedi voce)

* digitare:

rm -fr .cache/sessions/ .config/xfce4/

oppure:

* Visualizzare i file nascosti

* Doppio click  sulla cartella .cache , quindi cancellare la cartella sessions
* Doppio click  sulla cartella .config , quindi cancellare la cartella xfce4







