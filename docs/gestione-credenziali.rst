****************************************
La gestione delle credenziali di accesso
****************************************

Obiettivi generali
==================

La  protezione  delle  credenziali  di  accesso  rappresenta  uno  dei  principi fondamentali della sicurezza  delle  informazioni,  in  particolare  la  creazione  e  la  gestione  delle password che costituiscono la principale contromisura agli accessi non autorizzati.

Visto quanto previsto dall’attuale codice in materia di protezione dei dati personali D.Lgs. 196/03 - e, successivamente, ripreso dal nuovo regolamento europeo in vigore dal 24/05/2018, relativo alla protezione delle persone fisiche con riguardo al trattamento dei dati personali - GDPR  UE  2016/679,  occorre  definire  misure di protezione  adeguate ed  idonee  per  il  trattamento  e  la tutela  dei  dati  personali  degli utenti.

L'accesso agli elaboratori installati con il sistema operativo FUSS prevedono come strumento di accesso  per  gli  utenti,  un  sistema  di  autenticazione  (e  di  autorizzazione)  basato  su credenziali di accesso. Esso  consiste  in  un  codice  per  l’identificazione  dell’utente  (“username”  o  “nome utente”),  associato  ad  una  parola  chiave  riservata  (“password”)  conosciuta 
esclusivamente  dal  solo  utente.  I due  elementi,  uniti  insieme,  costituiscono  la credenziale  di  accesso  (“account”  o “utenza”)  così  come  definito  dalla  normativa vigente in tema di dati personali.

Responsabilità degli utenti
===========================

Gli utenti si impegnano a rispettare i criteri di creazione, conservazione e gestione delle credenziali di accesso di seguito indicati.

Gli  utenti,  una  volta  in  possesso  delle  credenziali,  devono  cambiare  la  password  al primo accesso rispettando i criteri di seguito descritti, evitando combinazioni facili da identificare.  Devono  scegliere  password  univoche,  che  abbiano  un  senso  solo  per l’utente che le sceglie, evitando di usare la stessa password per altre utenze.

La  password  è  strettamente  personale  e  non  deve  essere  comunicata  e/o  condivisa con nessun’altra persona all’interno o all'esterno della scuola.

Gli  utenti  devono  prestare  attenzione  a  fornire  le  proprie  credenziali  di  accesso,  a rispondere ad e - mail sospette e/o a cliccare sui link durante la navigazione web (o nella mail)  al  fine  di  contrastare  possibili  frodi  informatiche  (come  il  phishing,  lo  spear phishing, il furto d’identità, ecc.).

Ogni utente è responsabile di tutte le azioni e le funzioni svolte dal suo account. Qualora  vi  sia  la  ragionevole  certezza  che  le 
credenziali  assegnate  siano  state utilizzate da terzi, l’utente dovrà cambiare immediatamente la password. Per  la  conservazione  sicura  delle  credenziali  di  accesso  è  consigliabile  usare  un software  di  gestione  delle  password  (es.  KeePass,  LastPass,  ecc.)  evitando  di memorizzarle su fogli di carta, documenti cartacei e file conservati all’interno della postazione di lavoro. 

Qualora l’utenza venga bloccata a seguito della scadenza della password, è possibile cambiarla con la procedura descritta sotto. Nel caso in cui la password sia stata dimenticata, è necessario contattare il servizio di assistenza tecnica o l’amministratore di sistema.

Requisiti tecnici per la creazione e gestione delle password
============================================================

Come regola generale, la password deve essere ragionevolmente complessa e difficile da individuare e/o ricavare.

Nei limiti tecnici consentiti dai sistemi, la password:

1. deve essere di lunghezza non inferiore ad 8 caratteri;
2. deve essere  obbligatoriamente  cambiata  al  primo  utilizzo  e  successivamente almeno ogni 3 (tre) mesi;
3. deve contenere, ove possibile, almeno 3 caratteri tra numeri, caratteri alfabetici in maiuscolo e minuscolo, e caratteri speciali (es. C@p13nZa);
4. deve differire dalla precedente password per almeno tre caratteri;
5. non  deve  presentare  una  sequenza  di  caratteri  identici  o  gruppi  di  caratteri ripetuti;
6. non deve contenere riferimenti agevolmente riconducibili all’utente o ad ambiti noti;
7. non deve essere basata su nomi di persone, date di nascita, animali, oggetti o parole  ricavabili  dal  dizionario  (anche  straniere)  o  che  si  riferiscano  ad informazioni personali;

Ove tecnicamente possibile, i requisiti di cui ai punti da 1) a 5) vengono imposti da meccanismi automatici del sistema.

Per motivate  necessità di  urgente  accesso  alle  informazioni,  in  caso  di  impedimento del  titolare  delle  credenziali,  la  password  può  essere  annullata  e  sostituita  dagli amministratori di sistema con una nuova password. In  questo  caso  la  nuova password dovrà essere consegnata dall’amministratore di sistema all’utente, il quale dovrà modificarla al primo accesso.

Cambio password prima della scadenza
=========================================

È stato recentemente aggiunto un sistema di controllo che avvisa l'utente circa 10 giorni prima della scadenza  della password (per il gruppo *docenti* è di 90 giorni) . A questo punto l'utente viene guidato da messaggi intuitivi al cambio della stessa.
Se non si procede col cambio password, il messaggio verra ripetuto ad ogni login fino alla naturale scadenza (che comunque si sconsiglia di attendere). 

Il cambio password può essere effettuato ad ogni modo in qualunque momento come segue:

1. Aprire dal menu in basso a sinistra il  programma **Cambia la tua password** 
   (è sufficiente digitare "cambia" nel campo di ricerca).

.. figure:: images/change-pwd/password-change-1.jpg
    :width: 396px
    :align: center

2. Appare una finestra che  richiede la password attuale. Inserirla e cliccare su :guilabel:`OK`.

.. figure:: images/change-pwd/password-change-2.png
    :width: 396px
    :align: center

3. Inserire poi due volte la nuova password e cliccare su :guilabel:`OK`. 

.. figure:: images/change-pwd/password-change-3.png
    :width: 400px
    :align: center

.. figure:: images/change-pwd/password-change-4.png
    :width: 400px
    :align: center

4. Nel caso il cambio password non sia andato a buon fine appare una finestra che avvisa dell'errore e viene proposto di riprovare:

.. figure:: images/change-pwd/password-change-6.png
    :width: 400px
    :align: center

5. Nel caso il cambio password sia andato a buon fine apparirà il messaggio:

.. figure:: images/change-pwd/password-change-5.png
    :width: 400px
    :align: center


Cambio password dopo la scadenza
=====================================

Se la propria password è scaduta senza che si sia potuto aggiornarla dalla sessione utente con l'apposita applicazione, dopo essersi autenticati il sistema risponderà con il messaggio "*You are required to change your password immediately (password aged)*" che significa "*Si richiede di cambiare la password immediatamente (password scaduta)*".

.. figure:: images/aged-pwd/pwd-aged.png
    :width: 500px
    :align: center

Il sistema vi richiederà di inserire la nuova password che dovrà seguire le regole descritte nei requisiti esposti sopra. Se la password è troppo semplice il sistema mostrerà un messaggio richiedendovi di inserire una password più forte. Di seguito due esempi. 

.. figure:: images/aged-pwd/short-pwd.png
    :width: 500px
    :align: center

    Password troppo corta

.. figure:: images/aged-pwd/dictionary-pwd.png
    :width: 500px
    :align: center

    Password comune basata su un termine di dizionario.

Il sistema offre solo tre tentativi per inserire la nuova password; dopo il terzo tentativo si dovrà ripartire daccapo inserendo nuovamente il nome utente e la vecchia password.

Se invece la nuova password corrisponde ai requisiti, verrà richiesto di ridigitarla con il messaggio "*Retype new password*".

.. figure:: images/aged-pwd/msg-retype-pwd.png
    :width: 500px
    :align: center

.. figure:: images/aged-pwd/retype-pwd.png
    :width: 500px
    :align: center

Dopo averla digitata due volte correttamente, sarà possibile accedere al sistema. In caso contrario l'utente verrà avvisato che le password inserite non corrispondono: "*Sorry, passwords do not match*".

.. figure:: images/aged-pwd/pwds-not-matching.png
    :width: 500px
    :align: center

Il cambio password per i docenti viene richiesto in occasione del primo accesso al sistema e periodicamente ogni 90 giorni.
