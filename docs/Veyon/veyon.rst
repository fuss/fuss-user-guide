Introduzione
================

Veyon è un’applicazione che consente di monitorare e controllare un gruppo di computer (ad esempio aule) su un computer centrale (ad esempio il computer di un docente) e utilizzare varie funzionalità e modalità.


Avvio e accesso del programma
--------------------------------------

.. index:: Avvio da menu

Gli utenti non appartenenti al gruppo veyon-master verranno bloccati con un messaggio di errore.
Per gli utenti che appartengono al gruppo ``veyon-master`` il programma viene avviato tramite il menu di Avvio:	

.. image:: images/veyon/Menudiavvio.png
   :align: center

.. index:: Login, Username, Password

Per accedere verranno richiesti il nome utente e la password:

.. image:: images/veyon/LoginMaster.png
   :align: center

Inserire nome utente e password. Se i dati inseriti sono corretti  è possibile eseguire un login ed il programma verrà avviato. Altrimenti l’accesso verrà negato con un messaggio di errore. In tal caso si può riprovare ad accedere inserendo i dati corretti.

.. _demo-service:

All'avvio del programma compare una finestra di informazione che chiede se si voglia utilizzare la Funzione Demo; per poter presentare il proprio schermo è necessario cliccare su OK o anche semplicemente chiuderla. 

.. image:: images/veyon/demo-info.png
   :align: center

Se non serve subito si consiglia di lasciare la finestra in secondo piano. Nel momento in cui si attiva questa funzione si diventa temporaneamente "controllabili" da un'altra postazione (si potrà notare una specie di occhio sul pannello in basso a destra).
Per interrompere il servizio basta cliccare su OK nella finestra:

.. image:: images/veyon/demo-stop.png
   :align: center


Interfaccia utente
----------------------

.. index:: User interface, Toolbar, Monitor view, Status bar

Dopo l’avvio del programma si vedrà l’interfaccia utente con la barra degli strumenti (pannello superiore), la vista monitor (parte centrale) e la barra di stato con vari controlli (pannello inferiore)

.. image:: images/veyon/Interfaccia-utente-master.png
   :align: center

La barra degli strumenti contiene numerosi pulsanti per l’attivazione di diverse funzioni. Una descrizione dettagliata delle singole funzioni è disponibile nel capitolo  :ref:`Funzionalità del programma <ProgramFeatures>`. 

L’aspetto e il comportamento della barra degli strumenti possono essere personalizzati come descritto nella sezione :ref:`Barra degli strumenti <Toolbar>` .

Nella vista monitor i computer sono visualizzati in una vista a riquadri. A seconda della configurazione del sistema e dell’avvio del programma precedente, è già possibile visualizzare i computer nella posizione corrente qui. 

Il pannello selezione computer ti permette di mostrare o nascondere singoli computer o intere posizioni.

Gli elementi nella barra di stato vengono utilizzati per controllare l’interfaccia del programma e sono descritti in dettaglio nella sezione seguente.


Barra di stato
---------------------

Usando i pulsanti :guilabel:`Posizioni  & computers` and :guilabel:`Screenshots` , si possono aprire e chiudere il pannello :ref:`Selezione computer <ComputerSelectPanel>` e il pannello: :ref:`Screenshot <ScreenshotsPanel>` .

La barra di ricerca consente di filtrare i computer visualizzati utilizzando nomi di computer o nomi utente come termini di ricerca.  

Il pulsante |powered-on| (:guilabel:`Mostra solo computer accesi`)  nasconde tutti i computer spenti, disconnessi o non raggiungibili per qualche altro motivo. 

Utilizzare il dispositivo di scorrimento per controllare le dimensioni degli schermi dei computer visualizzati. Tenendo premuto quindi tasto Ctrl, la dimensione può anche essere cambiata usando la rotellina del mouse. La dimensione viene regolata automaticamente facendo clic sul pulsante |zoom-fit-best| (:guilabel:`Utilizzare il dispositivo di scorrimento per controllare le dimensioni degli schermi dei computer visualizzati. Tenendo premuto quindi tasto Ctrl, la dimensione può anche essere cambiata usando la rotellina del mouse. La dimensione viene regolata automaticamente facendo clic sul pulsante`) a destra.

È anche possibile utilizzare una disposizione del computer personalizzata, ad es. rappresentare l’attuale disposizione dei computer nelle aule. Dopo aver fatto clic sul pulsante |exchange-positions-zorder| (:guilabel:`Usa disposizione computer personalizzata`) ogni computer singolarmente o una selezione di computer possono essere spostati con il tasto sinistro del mouse premuto e disposti come desiderato. Per allineare tutti i computer nella disposizione personalizzata,
fare clic sul pulsante| ( :guilabel:`Allinea computer alla griglia`) . Se si desidera utilizzare nuovamente la disposizione standard ordinata, è sufficiente disattivare il pulsante  |exchange-positions-zorder| .

Il pulsante  |help-about| (:guilabel:`Informazioni su`) apre una finestra di dialogo con informazioni su Veyon come versione, produttore e condizioni di licenza.

.. |zoom-fit-best| image:: images/veyon/zoom-fit-best.png
  :scale: 20%
  :align: middle

.. |align-grid| image:: images/veyon/align-grid.png
  :scale: 20%
  :align: middle

.. |exchange-positions-zorder| image:: images/veyon/exchange-positions-zorder.png
  :scale: 20%
  :align: middle

.. |powered-on| image:: images/veyon/powered-on.png
  :scale: 20%
  :align: middle

.. |help-about| image:: images/veyon/help-about.png
  :scale: 20%
  :align: middle


.. _Toolbar:

Barra degli strumenti
---------------------------

È possibile personalizzare l’aspetto e il comportamento della barra degli strumenti. Un clic destro su una sezione libera o un pulsante apre un menu di scelta rapida con diverse voci:

.. image:: images/veyon/toolbar-contextmenu.png
   :align: center

Se si fa clic sulla voce  :guilabel:`Disabilita descrizioni comandi a palloncino` non verranno più visualizzate descrizioni comandi ogni volta che si passa il mouse sui pulsanti. Puoi aprire il menu contestuale in qualsiasi momento e deselezionare nuovamente l’elemento.

L'opzione :guilabel:`Mostra solo icone` offre una vista compatta dei pulsanti della barra degli strumenti nascondendo le etichette e visualizzando solo le icone. Su schermi più piccoli questa opzione potrebbe essere necessaria per visualizzare tutti i pulsanti.

.. _ComputerSelectPanel:

Pannello di selezione dei computer
---------------------------------------

.. index:: Computer select panel

Il pulsante :guilabel:`Posizioni & computer` nella barra di stato apre il pannello di selezione dei computer. Questo pannello visualizza tutte le posizioni dei computer disponibili in una struttura ad albero. È possibile espandere le singole voci della posizione facendo clic sul simbolo corrispondente davanti a loro.

Puoi attivare per il controllo singoli computer o intere aule . Tutti i computer controllati verranno quindi visualizzati nella vista di monitoraggio.

.. image:: images/veyon/computer-room-management.png
   :align: center

Con il pulsante :guilabel:`Salva elenco computer/utenti`  è possibile salvare l’elenco dei computer e gli utenti registrati in un file CSV. Tipici casi d’uso per questo sono i successivi controlli di presenza o esami basati sull’IT.


.. _ScreenshotsPanel:

Pannello Schermate
------------------------------

.. index:: Screenshots panel

Utilizzando il pannello di gestione degli screenshot, è possibile visualizzare ed eliminare tutti gli screenshot acquisiti. Le informazioni su come acquisire schermate sono fornite in :ref:`Funzionalità del programma <ProgramFeatures>` , capitolo nella sezione :ref:`Screenshot <FeatureScreenshot>`.

.. image:: images/veyon/ScreenshotManagementPanel.png
   :align: center

Ora puoi selezionare singoli screenshot dall’elenco. I dettagli dello screenshot, come la data di acquisizione, il nome utente e il computer, vengono quindi visualizzati nella tabella seguente. Il pulsante :guilabel:`Mostra` o un doppio clic nell’elenco mostra lo screenshot selezionato a schermo intero. Se non hai più bisogno dello screenshot, puoi eliminarlo definitivamente usando il pulsante :guilabel:`Elimina` . Si noti che questo processo non può essere annullato e i file non verranno spostati nel cestino.




.. _ProgramFeatures:

Funzionalità del programma
===========================

Veyon offre una varietà di funzioni che ti consentono di controllare e accedere ai computer. Tutte le funzionalità disponibili sono accessibili tramite i pulsanti nella barra degli strumenti e il menu di scelta rapida dei singoli computer.

Se muovi il mouse sui singoli pulsanti della barra degli strumenti, viene visualizzato un suggerimento con un breve testo di aiuto, a meno che tu non abbia disabilitato i suggerimenti. Premendo un pulsante si attiva la funzione desiderata su tutti i computer visualizzati.


.. _NonGlobalFeatureActivation:

Utilizzo delle funzioni su singoli computer
-----------------------------------------------------

Se si desidera attivare solo una funzione su un singolo computer, fare clic con il pulsante destro del mouse sul computer nella vista monitor e selezionare la funzione desiderata dal menu di scelta rapida. Le voci nel menu contestuale vengono visualizzate in modo dinamico in base alle funzioni attive.

.. image:: images/veyon/ContextMenu.png
   :scale: 75 %
   :align: center

Puoi anche selezionare più computer nella vista monitor disegnando un rettangolo di selezione con il mouse che include tutti i computer desiderati:

.. image:: images/veyon/select-computers.png
     :scale: 25 %
     :align: center

In alternativa, puoi premere il tasto :kbd:`Ctrl` e aggiungere i computer individualmente alla selezione con il clic del mouse.


Modalità di monitoraggio
-----------------------------------

.. index:: Monitoring mode, Monitoring, Observation, Computer overview

Per impostazione predefinita, Veyon è in esecuzione in modalità monitoraggio. In questa modalità hai una panoramica di tutti i computer e vedi i loro contenuti dello schermo nelle miniature. Il contenuto dello schermo viene aggiornato quasi in tempo reale, quindi è possibile monitorare tutte le attività nelle posizioni selezionate. 

Finché non è presente alcuna connessione a un computer, viene visualizzata l’icona di un computer anziché il contenuto dello schermo. Dopo l’avvio del programma, l’icona inizialmente è colorata di grigio. Non appena il programma rileva che il computer non è raggiungibile o l’accesso viene negato, il colore diventa rosso.

Alcune delle funzionalità descritte nelle sezioni successive cambiano i computer remoti in una modalità diversa. È possibile uscire dalla rispettiva modalità attivando di nuovo la modalità di monitoraggio.

.. image:: images/veyon/FeatureMonitoringMode.png
   :align: center


Modalità dimostrativa
--------------------------

.. index:: Demonstration mode, Fullscreen demo, Window demo, Demo mode, Demonstration, Presentation, Screen broadcast

È possibile utilizzare la modalità dimostrativa (modalità demo) per avviare una presentazione. In questa modalità, il contenuto del tuo schermo o di uno studente viene trasmesso a tutti i computer e visualizzato in tempo reale. Puoi scegliere tra uno schermo intero e una finestra demo.

Per avviare una presentazione dal tuo monitor è necessario avviare prima un servizio. Puoi farlo utilizzando l'apposita finestra di avvio :ref:`Avvio del servizio demo <demo-service>`, oppure, se è già stata chiusa, avviando il programma Demo nel Menu di avvio in basso a sinistra. 

.. image:: images/veyon/Demo.png
   :align: center


Durante una demo a schermo intero, il contenuto dello schermo verrà visualizzato a schermo intero sui computer remoti. Gli utenti che hanno effettuato l’accesso non possono utilizzare i propri computer per altre attività in questa modalità perché tutti i dispositivi di input sono bloccati. In questo modo otterrai la piena attenzione dei tuoi studenti.

Al contrario, una finestra demo consente agli utenti di alternare tra la finestra demo e le proprie applicazioni. Ad esempio, i partecipanti al corso possono disporre le finestre fianco a fianco e provare in parallelo i passaggi dimostrati. I dispositivi di input non sono quindi bloccati in questa modalità.

Per avviare una dimostrazione, devi aprire il menu demo premendo :guilabel:`Demo`:

.. image:: images/veyon/DemoMenu.png
   :align: center

Ora fai clic sulla voce desiderata nel menu. Se desideri condividere lo schermo di uno studente, assicurati di selezionarlo prima in modo che il programma sappia quale schermo trasmettere.

Nel caso in cui il tuo computer sia dotato di più schermi, puoi scegliere di trasmettere solo uno degli schermi. A tale scopo, fare clic sull’elemento dello schermo corrispondente nel menu demo prima di condividere lo schermo.

Se vuoi uscire di nuovo dalla modalità dimostrativa, premi semplicemente il pulsante o fai clic sul pulsante :guilabel:`Monitoring` per tornare alla modalità di monitoraggio a livello globale. Il menu di scelta rapida può essere utilizzato anche per interrompere la modalità dimostrativa sui singoli computer. 


Schermate di blocco
-----------------------------

.. index:: Screen lock, Lock screen, Lock computers, Lock input devices

Un altro modo per attirare l’attenzione degli studenti è utilizzare la funzione di blocco dello schermo. Come durante una dimostrazione a schermo intero, tutti i dispositivi di input sui computer degli studenti sono bloccati. I computer non possono quindi più essere utilizzati dagli studenti. Inoltre, viene visualizzato un simbolo di blocco per evitare distrazioni causate da applicazioni aperte.

Premere il tasto  :guilabel:`Blocca`  per bloccare tutti i computer visualizzati:

.. image:: images/veyon/FeatureScreenLock.png
   :align: center

Se si desidera sbloccare le schermate, è sufficiente premere nuovamente il pulsante o fare clic sul pulsante :guilabel:`Monitoring` per tornare alla modalità di monitoraggio a livello globale.

Se solo i singoli computer devono essere bloccati, è possibile selezionarli come descritto nella sezione :ref:`NonGlobalFeatureActivation` e selezionare la funzione di blocco dello schermo nel menu di scelta rapida. Il blocco dello schermo può quindi essere disattivato selezionando :guilabel:`sblocco` o tornando a modalità :guilabel:`Monitoraggio` . Il blocco dello schermo può anche essere attivato inizialmente a livello globale e successivamente disattivato per i singoli computer tramite il menu di scelta rapida.

.. note:: A causa delle restrizioni di sicurezza della maggior parte dei sistemi operativi, la schermata di blocco non può essere visualizzata se nessun utente è connesso. I dispositivi di input sono ancora bloccati, quindi non è possibile accedere dall’utente.


Accesso remoto
---------------------

.. index:: Remote access, Remote control, Remote view

l gruppo di funzioni ``accesso remoto`` è costituito da due funzioni molto simili :guilabel:`Vista remota` and :guilabel:`Controllo remoto`. In entrambe le modalità di accesso, lo schermo di un computer remoto viene visualizzato a schermo intero in una finestra separata. Contrariamente alla modalità di monitoraggio nella finestra principale, puoi guardare le attività su un computer in dettaglio o intervenire da solo.In entrambe le modalità di accesso, lo schermo di un computer remoto viene visualizzato a schermo intero in una finestra separata. Contrariamente alla modalità di monitoraggio nella finestra principale, puoi guardare le attività su un computer in dettaglio o intervenire da solo.

.. image:: images/veyon/FeatureRemoteAccess.png
   :align: center

Dopo aver premuto questo pulsante, viene aperta una finestra di dialogo che richiede l’accesso al nome host del computer:

.. image:: images/veyon/RemoteAccessHostDialog.png
   :scale: 75 %
   :align: center

Successivamente si apre una nuova finestra con la vista di accesso remoto:

.. image:: images/veyon/RemoteAccessWindow.png
   :scale: 75 %
   :align: center

Lo schermo remoto viene quindi visualizzato in pochi secondi e aggiornato in tempo reale. Nella parte superiore della finestra vedrai una barra degli strumenti con pulsanti simili all’applicazione principale. La barra degli strumenti scompare automaticamente alcuni secondi dopo aver stabilito la connessione. Puoi mostrarlo di nuovo in qualsiasi momento spostando il puntatore del mouse nella parte superiore dello schermo.

È inoltre possibile modificare la modalità di accesso in qualsiasi momento durante una sessione di accesso remoto in esecuzione. Tutto quello che devi fare è cliccare sul pulsante :guilabel:`Controllo remoto` or :guilabel:`Osserva solo` . Il pulsante non mostra la modalità di accesso corrente, ma la modalità di accesso che viene modificata quando viene premuto.

Non appena si accede alla modalità :guilabel:`Controllo remoto`, i tasti, i movimenti del mouse e i clic vengono trasmessi al computer remoto in modo da poterlo utilizzare come al solito. Tuttavia, a seconda del sistema operativo, alcuni tasti speciali o scorciatoie da tastiera come :kbd:`Ctrl+Alt+Del` non possono essere usati direttamente.  Se si desidera utilizzare queste scorciatoie, è possibile utilizzare il pulsante :guilabel:`Invia scorciatoia`.  Facendo clic su questo pulsante si apre un menu in cui è possibile selezionare il collegamento desiderato:

.. image:: images/veyon/RemoteAccessShortcutsMenu.png
   :align: center

Se il menu è stato aperto accidentalmente, può anche essere nuovamente chiuso senza innescare un’azione facendo nuovamente clic sul pulsante o premendo il tasto :kbd:`Esc`.

Se vuoi passare alla modalità a schermo intero, puoi usare il pulsante :guilabel:`Schermo intero` . In modalità schermo intero, l’etichetta del pulsante cambia in  :guilabel:`Finestra`. Puoi tornare facilmente alla modalità finestra facendo di nuovo clic su di essa.

La funzione :guilabel:`Screenshot` cattura il contenuto dello schermo corrente e lo salva in un file che può essere visualizzato in seguito. Maggiori informazioni sugli screenshot sono disponibili nelle sezioni  :ref:`Pannello Screenshot <ScreenshotsPanel>` and :ref:`Screenshot <FeatureScreenshot>`.

Con il pulsante :guilabel:`Uscita` la finestra di accesso remoto viene chiusa.


Riavviare e spegnere i computer
-------------------------------------------

.. index:: Power on, Turn on, Switch on, Power down, Shutdown, Turn off, Restart, Reboot, WoL, Wake-on-LAN

La funzione :guilabel:`Accendi` non è al momento funzionante nelle reti FUSS, nonostante sia presente nella barra degli strumenti.

Utilizzando le funzioni :guilabel:`Riavvia il PC` e :guilabel:`Spegni` , è possibile  riavviare o spegnere i computer controllati in remoto. I pulsanti corrispondenti sono disponibili nella barra degli strumenti:

.. image:: images/veyon/FeaturePowerControl.png
   :align: center

Fare clic sul pulsante appropriato per riavviare o spegnere tutti i computer visualizzati. Se si desidera utilizzare una funzione solo per singoli computer, è possibile selezionare i rispettivi computer e selezionare l’elemento desiderato nel menu di scelta rapida (tasto destro del mouse).

Veyon 4.2 e versioni successive offrono ulteriori opzioni di spegnimento. Queste opzioni possono essere selezionate da un menu che si apre facendo clic sul pulsante :guilabel:`Spegni` :

.. image:: images/veyon/PowerDownOptions.png
   :align: center

Sono disponibili le seguenti azioni:

Spegni ora
    I computer si spegneranno immediatamente senza ulteriori finestre di dialogo di conferma.

Installa gli aggiornamenti e spegni
    Se supportato dal sistema operativo, tutti gli aggiornamenti di sistema disponibili verranno installati durante il processo di spegnimento. Se non sono disponibili aggiornamenti, il rispettivo computer verrà spento immediatamente.

Spegni dopo la conferma dell’utente
    Con questa opzione, ad ogni utente connesso verrà chiesto se spegnere il rispettivo computer. Se nessun utente è connesso a un determinato computer, verrà spento immediatamente.

Spegni dopo il timeout
    Con questa opzione, ad ogni utente connesso verrà richiesto se chiudere il rispettivo computer. Se nessun utente è collegato a un determinato computer, verrà spento immediatamente.
    
    .. image:: images/veyon/PowerDownTimeInputDialog.png
       :align: center
    
    Dopo aver accettato la finestra di dialogo, su tutti i computer viene visualizzata una finestra di conto alla rovescia che dice agli utenti di salvare il proprio lavoro e chiudere tutte le applicazioni.

.. attention:: Si noti che, a seconda della configurazione del programma, i computer verranno riavviati o arrestati senza ulteriori finestre di dialogo di conferma. Pertanto, assicurarsi sempre che gli utenti che hanno effettuato l’accesso abbiano salvato tutti i documenti aperti ecc. E, se possibile, abbiano chiuso tutti i programmi. Ciò impedisce la perdita indesiderata di dati.


Log in utente
------------------

.. index:: Log in, Login, Logon, Log on

La funzione: :guilabel:`Log in`  per ragioni di sicurezza non è attualmente disponibile nelle reti scolastiche.

.. image:: images/veyon/FeatureUserLogin.png
   :align: center



.. _LogOffUsers:

Log off utenti
---------------------

.. index:: Log off, Log out, User log out, Log off users, End of lesson

La funzione :guilabel:`Disconnetti` completa le possibilità descritte nella sezione precedente per controllare gli stati di base del computer. A tale scopo è disponibile un pulsante corrispondente nella barra degli strumenti:

.. image:: images/veyon/FeatureUserLogoff.png
   :align: center

Attiva questo pulsante per disconnettere tutti gli utenti da tutti i computer controllati. Se si desidera utilizzare questa funzione solo per singoli computer, è possibile selezionare i rispettivi computer e selezionare l’elemento desiderato nel menu di scelta rapida (tasto destro).

.. hint:: Dopo aver accettato la finestra di dialogo, su tutti i computer viene chiusa una finestra di conto alla rovescia che dice agli utenti di salvare il proprio lavoro e chiudere tutte le applicazioni.

.. attention:: Si noti che gli utenti che hanno effettuato l’accesso, a seconda della configurazione del programma, vengono disconnessi senza ulteriori finestre di dialogo di conferma. Pertanto, assicurarsi sempre che gli utenti che hanno effettuato l’accesso abbiano salvato tutti i documenti aperti ecc. E, se possibile, abbiano chiuso tutti i programmi. Ciò impedisce la perdita indesiderata di dati.


Invia un messaggio di testo
-------------------------------------

.. index:: Text message, Message, Message window

Un’altra possibilità per l’interazione dell’utente è inviare un messaggio di testo a singoli o a tutti i partecipanti al corso. Il messaggio di testo viene visualizzato sui computer sotto forma di finestra di messaggio. Il pulsante :guilabel:`Invia messaggio` è disponibile a questo scopo:

.. image:: images/veyon/FeatureTextMessage.png
   :align: center

Dopo aver premuto il pulsante, si apre una finestra di dialogo in cui è possibile inserire il messaggio da visualizzare:

.. image:: images/veyon/TextMessageDialog.png
   :align: center

Fare clic su :guilabel:`OK` per inviare il messaggio inserito.

Se si desidera utilizzare questa funzione solo per singoli computer, è possibile selezionare i rispettivi computer e selezionare l’elemento desiderato nel menu di scelta rapida.


Apri un programma
----------------------------

.. index:: Run program, Start program, Execute programm, Commands, Open document

Se un programma specifico deve essere avviato su tutti i computer, è possibile utilizzare la funzione  :guilabel:`Esegui programma` nella barra degli strumenti. A tale scopo, fare clic sul pulsante mostrato:

.. image:: images/veyon/FeatureRunProgram.png
   :align: center

A seconda che siano stati aggiunti programmi personalizzati prima o che i programmi siano stati predefiniti dall’amministratore, si aprirà un menu a comparsa o una finestra di dialogo. Nel primo caso, tutti i programmi disponibili sono elencati nel menu:

.. image:: images/veyon/RunProgramMenu.png
   :align: center

Fare clic sull’elemento desiderato per avviare il rispettivo programma su tutti i computer. In alternativa, fare clic sull’ultimo elemento  :guilabel:`Programma personalizzato` per eseguire un programma non elencato. Questo aprirà una nuova finestra di dialogo. In questa finestra di dialogo è possibile inserire il nome del programma da eseguire:

.. image:: images/veyon/RunProgramDialog.png
   :align: center

Confermare questa finestra di dialogo con :guilabel:`OK` per eseguire il programma. 

.. note:: Per rimuovere un programma personalizzato aggiunto in precedenza, sposta il mouse sull’elemento corrispondente e premi il tasto :kbd:`Del` .

.. hint:: È possibile passare un argomento alla maggior parte dei programmi con il nome di un file che si desidera aprire automaticamente. Ad esempio, se si desidera riprodurre un video su tutti i computer, aggiungere il percorso al file video separato da uno spazio, ad es.  ``vlc Scrivania\Esempio.mp4``.

.. attention:: Nel caso in cui il percorso del programma o il nome del file contenga spazi (cosa in generale da evitare con Linux), non è al momento possibile eseguirlo o aprire il file.

Collegamento ad un sito internet
-------------------------------------------------

.. index:: Open website, Website, Open browser, Browser, URL, Web address

Se desideri che tutti gli studenti aprano un sito Web specifico, puoi automaticamente aprire quel sito Web su tutti i computer. Utilizzare il pulsante :guilabel:`Apri sito Web` per farlo:

.. image:: images/veyon/FeatureOpenWebsite.png
   :align: center

A seconda che siano stati aggiunti siti Web personalizzati in precedenza o che i siti Web siano stati predefiniti dall’amministratore, viene visualizzato un menu a comparsa o una finestra di dialogo. Nel primo caso, tutti i siti Web disponibili sono elencati nel menu:

.. image:: images/veyon/OpenWebsiteMenu.png
   :align: center

Fare clic sull’elemento desiderato per aprire il rispettivo sito Web su tutti i computer. In alternativa, fare clic sull’ultimo elemento :guilabel:`Sito Web personalizzato` per aprire un sito Web non elencato. Questo aprirà una nuova finestra di dialogo. In questa finestra di dialogo è possibile inserire l’indirizzo del sito Web da aprire:

.. image:: images/veyon/OpenWebsiteDialog.png
   :align: center

Confermare questa finestra di dialogo con :guilabel:`OK` per aprire il sito Web.

.. note:: Per rimuovere un sito Web personalizzato aggiunto in precedenza, spostare il mouse sull’elemento corrispondente e premere il tasto :kbd:`Del` .

Trasferimento di file
----------------------------

.. index:: Files, File transfer, Transfer files, Distribute files, Send files, Open files, Destination folder

Utilizzando la funzione di trasferimento file è possibile trasferire facilmente i file a tutti gli studenti e, successivamente, aprire i file trasferiti. Per prima cosa fai clic sul pulsante :guilabel:`Trasferimento di file` per aprire una finestra di dialogo che ti consente di selezionare i file da trasferire:

.. image:: images/veyon/FeatureFileTransfer.png
   :align: center

Dopo aver selezionato i file desiderati, si apre la finestra di dialogo per il trasferimento dei file:

.. image:: images/veyon/FileTransferDialogStart.png
   :align: center

In questa finestra di dialogo è possibile scegliere ulteriori opzioni prima di iniziare il trasferimento del file. Per impostazione predefinita, solo i file verranno trasferiti nella home o nella directory del profilo dell’utente senza sovrascrivere i file esistenti.

Sovrascrivi i file esistenti
    In questa finestra di dialogo è possibile selezionare ulteriori opzioni prima di iniziare il trasferimento del file. Per l’impostazione predefinita, solo i file verranno trasferiti nella home o nella directory del profilo dell’utente senza sovrascrivere i file esistenti.

Solo trasferimento
    In questa modalità, vengono trasferiti solo i file senza eseguire ulteriori azioni. Utilizzare questa modalità per distribuire silenziosamente il materiale didattico in anticipo senza disturbare gli studenti.

Trasferisci e apri i file(s) con il programma associato
    In questa modalità, i file trasferiti verranno aperti con il programma corrispondente associato al rispettivo tipo di file. Ad esempio, i documenti di testo verranno aperti con il programma di elaborazione testi installato. Utilizzare questa modalità per far lavorare immediatamente gli studenti con i materiali forniti.

Trasferisci e apri la cartella di destinazione
    Se si intende trasferire più file contemporaneamente, aprirli tutti automaticamente non è una buona scelta nella maggior parte dei casi. Invece, la cartella di destinazione può essere aperta in una finestra di gestione file in cui gli studenti possono visualizzare i file trasferiti e aprire quelli desiderati stessi.

Dopo aver scelto le opzioni desiderate fare clic sul pulsante :guilabel:`Start` per avviare il trasferimento dei file. A seconda della dimensione dei file e del numero di computer, questo potrebbe richiedere del tempo. Nella parte inferiore della finestra di dialogo viene visualizzata una barra di avanzamento con l’avanzamento totale. Dopo che i trasferimenti sono stati completati, puoi fare clic sul pulsante :guilabel:`Chiudi` per terminare:

.. image:: images/veyon/FileTransferDialogFinished.png
   :align: center


.. _FeatureScreenshot:

Screenshot
----------------

.. index:: Screenshot, Snapshot

Veyon ti consente di salvare il contenuto dello schermo corrente di singoli o tutti i computer in file di immagini. Facendo clic sul pulsante :guilabel:`Screenshot` si attiva la funzione per tutti i computer visualizzati:

.. image:: images/veyon/FeatureScreenshot.png
   :align: center

Se si desidera utilizzare questa funzione solo per singoli computer, è possibile selezionare i rispettivi computer e selezionare la voce :guilabel:`Screenshot` dal menu contestuale.

Riceverai quindi un messaggio informativo su quanti screenshot sono stati acquisiti. Ora puoi visualizzare le immagini nel pannello :ref:`Catture dello schermo  <ScreenshotsPanel>` ed eliminarli se non necessari.
