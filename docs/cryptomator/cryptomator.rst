Premessa
============================

Cryptomator è un'applicazione che permette di crittografare i tuoi dati in modo rapido e semplice, così puoi caricarli in sicurezza su un dispositivo esterno come una chiavetta USB oppure sul tuo servizio cloud preferito.
Con Cryptomator puoi creare “casseforti” crittografate . Ogni cassaforte è protetta da una password e può contenere tutti i file e le cartelle che desideri. Per approfodimenti e dettagli fai riferimento al sito ufficiale: `<https://docs.cryptomator.org/en/latest/>`_ .
Dopo che una cassaforte è stata sbloccata, deve essere integrata nel sistema per essere accessibile all'utente. Cryptomator utilizza tre diverse tecnologie (chiamate adattatori ) per questa integrazione.
Per funzionare correttamente su ``Fuss 10`` bisogna scegliere l'opzione ``WebDAV`` (su Fuss 11 si può scegliere anche ``FUSE`` ) .

1) Si clicchi sulla rotellina delle ``Preferenze`` nella finestra iniziale.

2) Si clicchi su ``Unità virtuale``

3) Si scelga ``WebDAV`` ; porta e schema vengono assegnati di default.

.. image:: images/unita_virtuale.png
   :align: center

Avvio e accesso al programma
=================================

.. index:: Avvio da menu

Il programma può essere avviato dal menu di avvio in basso a sinistra.

Quando accedi per la prima volta di Cryptomator, ovviamente non hai ancora una cassaforte. Se avvii l'applicazione, apparirà la seguente schermata:

.. image:: images/schermata_iniziale.png
   :align: center

Cliccando sul pulsante ``Aggiungi Cassaforte`` nell'angolo in basso a sinistra della finestra, puoi creare una nuova cassaforte.

Aggiunta di casseforti
============================

.. index:: Aggiunta di casseforti

.. image:: images/aggiungi_o_apri_cassaforte.png
   :align: center

Se vuoi aggiungere una nuova cassaforte, hai essenzialmente due opzioni:

    1. Se vuoi creare tu stesso una cassaforte, scegli ``Crea Nuova Cassaforte``

    2. Se hai già una cassaforte, ad esempio perché qualcuno l'ha condivisa con te tramite un servizio di archiviazione cloud, puoi invece scegliere ``Apri Cassaforte Esistente``. 

Crea una nuova cassaforte
-------------------------------------------------------

Se hai scelto di creare una nuova cassaforte, la procedura guidata  ti accompagnerà attraverso il processo di creazione:

- scegli un nome

- scegli una posizione di archiviazione (locale, su un device esterno come una chiavetta USB o in cloud)

- scegli una password (da non dimenticare, per sicurezza si può decidere di salvarsi una chiave di recupero)


.. attention:: Nessuno tranne te stesso conosce questa password e nessuno può "reimpostare" la tua password per te.


.. note:: Se prevedi di condividere questa cassaforte con un'altra persona, dovrete entrambi conoscere la password. In questo caso, scegli una password diversa da quella che tendi ad usare solo per te stesso. Per condividere la password, utilizzare un messaggio crittografato o qualsiasi altro mezzo di comunicazione sicuro. Poiché non è possibile reimpostare la tua password in caso la dimentichi, ti consigliamo di creare una chiave di ripristino aggiuntiva .


- Mostra chiave di ripristino (passaggio facoltativo)


Se hai scelto di creare una chiave di ripristino nel passaggio precedente, verrà ora visualizzata. Assicurati di non perderla e conservala con cura.

.. image:: images/chiave_recupero.png
   :align: center

Questo è tutto. Hai creato con successo una nuova cassaforte. Ora puoi sbloccarla usando la tua password e iniziare ad aggiungere file riservati.

.. image:: images/unlock_password.png
   :align: center

Aprire una cassaforte esistente
-------------------------------------------------------------

Se hai scelto di aprire una cassaforte esistente, tutto ciò che devi fare è individuare il file ``masterkey.cryptomator``  della cassaforte che desideri aprire.

.. note:: Se hai creato il deposito su un altro dispositivo e non riesci a trovarlo o il relativo file della chiave master, assicurati che la directory contenente il deposito sia sincronizzata correttamente e completamente accessibile sul tuo dispositivo.

Accesso alle Casseforti
===========================

Dopo aver aggiunto una cassaforte a Cryptomator, probabilmente vorrai sbloccarla, in modo da poter iniziare ad aggiungere file.

Sbloccare una cassaforte
--------------------------------------------------------
Per sbloccare la cassaforte selezionata, fai clic sul pulsante  ``Sblocca`` al centro della finestra di Cryptomator. Ti verrà quindi richiesta la password:

.. note:: Selezionando la casella di controllo "Salva password", la password verrà memorizzata nel portachiavi del tuo sistema operativo.

.. attention:: Archivia la tua password solo su dispositivi affidabili. Chiunque abbia accesso a questo computer potrà accedere alla tua cassaforte se la password è memorizzata nel portachiavi di sistema.

Se la password è corretta viene visualizzata una conferma. Puoi semplicemente chiudere questa finestra facendo clic su ``Fatto`` o facendo clic su ``Rivela l'Unità`` per mostrare la cassaforte sbloccata nel tuo file manager.

.. image:: images/rivela_unita.png
   :align: center



Lavorare con la cassaforte sbloccata
-----------------------------------------------------------------------------
Dopo lo sblocco, i contenuti della tua cassaforte saranno disponibili come unità virtuale sul tuo PC. Ciò significa che puoi interagire con i tuoi file riservati allo stesso modo di qualsiasi altro disco rigido o chiavetta USB.
Se non riesci a trovarela cassaforte sbloccata nel file manager del tuo sistema (Gestore dei file, Esplora risorse, …), puoi sempre fare clic su ``Rivela l'Unità`` nella finestra di Cryptomator.

Bloccare una cassaforte
------------------------------------------------------
Per bloccare nuovamente il tuo caveau, fai semplicemente clic su ``Blocca`` e  i tuoi dati saranno nuovamente protetti e crittografati.

Cambiare la password
============================
È possibile modificare la password di una cassaforte già esistente. L'unica cosa di cui hai bisogno è ricordare quella attuale.

.. note:: Quando cambi la password le chiavi crittografate la prima volta rimarranno le stesse. I file effettivi non verranno nuovamente crittografati. Se desideri crittografare i file della tua cassaforte con una nuova password più forte, devi creare una nuova cassafortee trascinare i dati dal vecchio al nuovo.

Per fare ciò, fai clic sul pulsante ``Modifica password`` nella scheda ``Password`` delle ``Opzioni della Cassaforte``. Nella finestra aperta vengono visualizzati tre campi di immissione di testo: 
 
    1. Nella prima è necessario inserire la password corrente.

    2. Il secondo prende la nuova password e, come già detto, suggeriamo di seguire le regole di creazione delle buone password.

    3. Nella terza è necessario inserire nuovamente la nuova password per conferma.

.. image:: images/cambia_password.png
   :align: center

Per procedere è necessario confermare ciò che si sta facendo spuntando la casella di controllo.
Per terminare e cambiare davvero la password, fai clic sul pulsante ``Modifica``.

.. note:: Solo se il secondo e il terzo campo di immissione del testo corrispondono e la casella di controllo è selezionata, il pulsante ``Modifica`` è attivato.

Chiave di ripristino
==========================

Mostrare la chiave di ripristino
----------------------------------------------------------------

Non è un problema se hai perso la visualizzazione della chiave di ripristino durante la creazione della cassaforte. Puoi ancora ricavarla e visualizzarla in un secondo momento. Per aumentare la sicurezza, Cryptomator non la memorizza sul disco rigido e lo ricava sempre "al volo".
.. attention:: Tieni presente che, grazie alla capacità della chiave di ripristino di reimpostare la password corrente, essa è altamente riservata. Assicurati che solo persone fidate vi abbiano accesso e tienila in un luogo sicuro.

Per fare ciò, fai clic su ``Visualizza la chiave di recupero`` nella scheda ``Password`` di ``Opzioni della Cassaforte`` e inserisci la tua password. Verrà aperta una nuova finestra. Mostra una sequenza di parole all'interno di un campo di testo. Questa sequenza è la chiave di ripristino della cassaforte.

.. image:: images/chiave_recupero.png
   :align: center

Puoi copiarla negli appunti o stamparla su carta. Se hai finito, chiudi la finestra con il pulsante ``Fatto``.


Resettare la password con la chiave di ripristino
----------------------------------------------------------------------------------

Se hai dimenticato la password per una cassaforte, ma hai salvato la chiave di ripristino in un luogo esterno, puoi creare una nuova password e accedere nuovamente alla cassaforte.
Passare a ``Password`` nella scheda nelle ``Opzioni della Cassaforte`` e fare clic sul pulsante ``Recupera password``. Viene aperta una nuova finestra che chiede di inserire la chiave di ripristino nella casella di testo mostrata. Inseriscila lì copiandola da un file o digitando.

.. image:: images/recupera_password.png
   :align: center

.. note:: Se hai stampato la tua chiave di ripristino su carta o l'hai archiviata da qualche parte dove non puoi copiarla, Cryptomator ti offre una funzione di completamento automatico per l'inserimento. Digita una lettera e verifica se la parola mostrata corrisponde alla tua parte chiave. In tal caso, puoi premere il tasto Tab o il tasto freccia destra per completare automaticamente la parola. Altrimenti inserisci più lettere, il suggerimento cambierà di conseguenza.

Se la chiave di ripristino è valida, Cryptomator lo segnala con un piccolo messaggio e attiva il pulsante ``Avanti``.

Nell'ultimo passaggio devi assegnare una nuova password alla tuo cassaforte. È la stessa procedura seguita durante la creazione della cassaforte, tranne per il fatto che non viene generata alcuna nuova chiave di ripristino. Si consiglia di seguire i suggerimenti sualla creazione di una buona password.

Termina la finestra di dialogo immettendo nuovamente la stessa password e facendo clic sul pulsante ``Fatto``. Ora puoi sbloccare la cassaforte con la nuova password.

.. note:: Poiché la chiave di ripristino rimane la stessa, non scartarla e riponila in un luogo sicuro.


Rimuovere le casseforti
=======================

È possibile rimuovere una cassaforte dall'elenco facendo clic con il pulsante destro del mouse sulla voce dell'elenco. Questo è possibile solo mentre la cassaforte è bloccata.

.. note:: Rimuovendo una cassaforte da questo elenco, essa non viene eliminata dal filesystem. Per eliminare irrevocabilmente la cassaforte puoi semplicemente eliminare la directory della stessa (che contiene la tua masterkey.cryptomator e la cartella d) usando il tuo normale ``File manager``.




