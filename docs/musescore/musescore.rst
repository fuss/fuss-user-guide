Premessa
====================

MuseScore è un software open-source per la creazione di partiture musicali. Ecco una panoramica delle sue caratteristiche principali:

1. **Interfaccia Utente Intuitiva**: MuseScore offre un'interfaccia utente chiara e intuitiva che consente agli utenti di creare partiture musicali senza necessariamente avere competenze avanzate nella notazione musicale.

2. **Notazione Musicale**: Puoi scrivere spartiti musicali complessi utilizzando una vasta gamma di simboli musicali, note, pause e altro ancora. Il programma supporta diversi strumenti musicali e varie chiavi di lettura.

3. **Riproduzione Audio**: Una delle caratteristiche più apprezzate di MuseScore è la sua capacità di riprodurre l'audio delle partiture. Puoi ascoltare in anteprima la tua composizione per valutare come suonerà.

4. **Importazione/Esportazione di File**: MuseScore supporta diversi formati di file, tra cui MusicXML e MIDI. Ciò consente agli utenti di importare partiture esistenti o esportare il proprio lavoro in vari formati compatibili con altri software musicali.

5. **Personalizzazione**: Puoi personalizzare l'aspetto delle partiture, ad esempio modificando lo stile della notazione, le dimensioni dei caratteri e altri dettagli visivi.

6. **Community e Condivisione**: MuseScore ha una comunità online attiva. Gli utenti possono condividere le proprie partiture, collaborare con altri musicisti e accedere a una vasta libreria di partiture già create.

7. **Compatibilità con Piattaforme Diverse**: MuseScore è disponibile per diverse piattaforme, tra cui Windows, macOS e Linux. Inoltre, è disponibile una versione online chiamata MuseScore.com che consente di creare e condividere partiture direttamente nel browser.


Come evitare le distorsioni (gracchiamento) nella riproduzione dell'audio
================================================================================================

Le seguenti istruzioni sono valide per ``Musescore 2``  e  ``Musescore 3``.
Nel caso si presentino problemi di distorsione del suono, si può risolvere il problema cambiando il sistema di gestione del suono.
Spostarsi in **Preferenze** > **I/O e suoni** e mettere la spunta a **Usa ALSA Audio** come nella figura seguente:


.. image:: images/preferenze_musescore.png
   :align: center

Perchè la modifica diventi efficace è necessario riavviare l'applicazione.

Un'altra possibile causa di distorsione potrebbe essere il volume troppo alto impostato lato applicazione (l'amplificazione in questo caso è software e non hardware).
Verificare che tale volume sia regolato correttamente.
Cliccare su **Visualizza** e spuntare **Controlli riproduzione** per aprire la finestra.

.. image:: images/controlli_riproduzione.png
   :align: center


