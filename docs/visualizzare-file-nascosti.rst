**************************************
Visualizzare file nascosti
**************************************

Premessa
===============


I file nascosti hanno la caratteristica che i loro nomi iniziano con un punto (.).
Tutti i file di configurazione specifici dell’utente sono memorizzati in file nascosti, in genere divisi per cartelle.
Essi si trovano tutti nella cartella home dell’utente.

Visualizzare i file nascosti
====================================

* Entrare nella propria home (da Desktop doppio click su Casa)
* Si apre la seguente finestra:

.. figure:: images/fn-1.png
    :width: 300px
    :align: center
	    
* Nella barra dei menu in alto selezionare Visualizzazione. Mettere un flag (o cliccare) sulla voce :menuselection:`Mostra i file nascosti`

.. figure:: images/fn-visualizzazione.png
    :width: 300px
    :align: center

* Ora la cartella mostrerà molti altri file e sottocartelle che prima non erano visibili







