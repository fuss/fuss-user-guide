*****************************************************************
Giochi non più supportati accessibili attraverso fuss-distrobox
*****************************************************************

Premessa
==================

Il programma **distrobox** consente di utilizzare più distribuzioni linux all’interno della stessa finestra di terminale.
Questo strumento è stato sfruttato nel pacchetto ``fuss-distrobox`` per creare un contenitore fuss 10 all’interno di un sistema fuss 12. Ciò ha permesso di installare dei giochi didattici come **Omnitux** e **Pysiogame**,  non più compatibili con le versioni più recenti di fuss, ed anche di utilizzare i giochi **Iprase**, che richiedono l’architettura i386, evitando di installare quest’ultima nel sistema principale.

Come si installa?
=========================

Si installa da terminale col comando: ::

    apt update && apt install fuss-distrobox


Come si accede ai giochi?
==============================

Per accedere ai giochi ci si deve loggare con l’account ``gioco:gioco`` (gioco è un utente locale che può accedere al container distrobox fuss 10).
I giochi si trovano nel menu in basso a sinistra, nella sezione Istruzione oppure digitando il loro nome nella casella di ricerca.

.. image:: images/giochi-menu.png
   :width: 200px
   :align: center

|

La finestra principale di Omnitux è la seguente:

.. image:: images/omnitux.png
    :width: 400px
    :align: center

|
   
La finestra principale di Pysiogame è la seguente:

.. image:: images/pysiogame.png
   :width: 400px
   :align: center

|

Le voci a Menu dei giochi Iprase sono invece due.

.. image:: images/menu-iprase.png
   :width: 200px
   :align: center

|

In ``Giochi IPRASE`` i giochi vengono lanciati a partire da una interfaccia principale.

.. image:: images/interfaccia-iprase.png
   :width: 400px
   :align: center

|

In ``Giochi Iprase extra`` essi si lanciano invece entrando nella specifica cartella e cliccando sulla loro icona.

.. image:: images/giochi_desktop.png
   :width: 400px
   :align: center







