********************************************
Operazioni principali sul desktop
********************************************

Apertura di un terminale
===========================

Click col tasto destro del mouse in un punto vuoto del Desktop: si apre il seguente menu contestuale. Cliccare su  :menuselection:`Apri un terminale qui`.

.. figure:: images/desktop/ctrl_dx.png
    :width: 200px
    :align: center


Aggiunta di un programma al pannello dei programmi principali
==============================================================================

* Aprire la finestra delle Applicazioni

* Selezionare il programma dal menu delle categorie a destra

* Tenendo premuto il tasto CTRL, trascinare l’icona del programma sul pannello inferiore in uno spazio vuoto. Se non si sbaglia, compare la seguente finestra:

.. figure:: images/desktop/avviatore.png
    :width: 300px
    :align: center

* Cliccare sul pulsante  :guilabel:`Crea avviatore` : l’icona dovrebbe comparire accanto a quelle dei programmi preferiti


Aggiunta di plugin al pannello inferiore 
==============================================================================

Cliccare col tasto destro in un punto vuoto del pannello inferiore. Si apre il seguente menu contestuale

.. figure:: images/desktop/avviatore.png
    :width: 200px
    :align: center


* Click su voce Pannello e si apre un sottomenu

* Click su Aggiungi nuovi elementi: si apre la seguente finestra:

* Ricercare il plugin o selezionarlo: esso verrà aggiunto al pannello dei plugin a destra in basso

Aggiunta selettore di spazi di lavoro
===========================================

* Seguire le istruzioni per aggiungere un plugin al pannello inferiore

* Selezionare il plugin :menuselection:`Selettore di spazi di lavoro`.

Si apre la seguente finestra:

.. figure:: images/desktop/spazi_lavoro.png
    :width: 200px
    :align: center
    
* Fare clic col tasto destro sul nuovo elemento creatosi nel pannello. Dal menu contestuale che si apre selezionare :menuselection:`Impostazione degli spazi di lavoro` , come da immagine sotto:
 
 .. figure:: images/desktop/impostazione_spazi.png
    :width: 200px
    :align: center
    
 
* Modificare il numero di spazi che si vogliono utilizzare come nell'immagine sottostante:

 .. figure:: images/desktop/numero_spazi.png
    :width: 200px
    :align: center
  
    
Fatta questa operazione, cliccare sul pulsante :guilabel:`Chiudi`

In conclusione, se per esempio avete scelto tre spazi di lavoro, nel pannello inferiore trovate gli elementi come nell'immagine qui sotto: 

 .. figure:: images/desktop/spazi.png
    :width: 200px
    :align: center

Potete passare con un clic del mouse su uno di essi per passare da uno spazio di lavoro all'altro.
