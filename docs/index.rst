FUSS - Manuale Utente
=====================

.. figure:: images/fuss.png
      :width: 100px
      :align: right

Il presente manuale guida l'utente all'utilizzo della versione live/desktop/client della distribuzione `FUSS <https://fuss.bz.it>`_ GNU/Linux nell'attuale versione 12 basata su Debian 12 "bookworm".

.. toctree::
   :maxdepth: 3

   il-desktop
   operazioni-sul-desktop
   visualizzare-file-nascosti
   impostazioni-linguistiche
   dispositivi-bluetooth
   ridimensionare-molte-immagini
   gestione-credenziali
   index-proxy
   problemi-vari
   index-veyon
   index-cryptomator
   index-musescore
   index-fuss-microscope
   index-fuss-distrobox

Contribuisci
""""""""""""

Chiunque può contribuire a migliorare questa documentazione che è scritta in `reStructuredText <http://www.sphinx-doc.org/rest.html>`_.

Supporto
""""""""

Se ti serve aiuto, scrivi una mail ad info@fuss.bz.it

Licenze
"""""""

.. image:: https://img.shields.io/badge/code-GPLv3-blue.png
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: Code GPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.png
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA

* :ref:`genindex`
