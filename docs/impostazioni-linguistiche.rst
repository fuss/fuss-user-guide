*************************
Impostazioni linguistiche
*************************

Cambio lingua dell'interfaccia
==============================

Dopo essersi autenticati è possibile cambiare la lingua del sistema scegliendo tra quelle disponibili. Si deve avviare da menu il programma :menuselection:`Set language for Next Session` presente nella categoria :menuselection:`Impostazioni`.

.. figure:: images/change-language/menu-set-language.png
    :width: 500px
    :align: center

Si aprirà una semplice finestra di impostazioni: è sufficiente selezionare la lingua desiderata e poi premere il bottone :guilabel:`Set locale`.

.. figure:: images/change-language/set-language.png
    :width: 341px
    :align: center

Infine è necessario uscire dall'interfaccia e rientrare per rendere effettive le modifiche e poter lavorare con la lingua desiderata.

.. figure:: images/change-language/exit.png
    :width: 364px
    :align: center


Layout della tastiera
=====================

La tastiera predefinita in FUSS è quella italiana e l'indicatore :guilabel:`IT` è presente nell'area di notifica nel pannello inferiore a destra.

.. figure:: images/kbd-layout/tastiera-1.png
    :width: 95px
    :align: center
	    
È possibile aggiungere altre tastiere cliccando il tasto destro del mouse e selezionando :menuselection:`Preferenze`.

.. figure:: images/kbd-layout/tastiera-2.png
    :width: 187px
    :align: center
       
Si apre la finestra di Preferenze nella quale va scelta la scheda :menuselection:`Metodo di input`.

.. figure:: images/kbd-layout/tastiera-3.png
    :width: 655px
    :align: center
	    
Con il bottone :guilabel:`Aggiungi` si possono selezionare altre tastiere virtuali oltre a quella preimpostata. Nell'esempio aggiungiamo la tastiera tedesca che verrà inserita di seguito a quella italiana.

.. figure:: images/kbd-layout/tastiera-4.png
    :width: 272px
    :align: center
	    
.. figure:: images/kbd-layout/tastiera-5.png
    :width: 655px
    :align: center
	    
Similmente con il :guilabel:`Rimuovi` è possibile rimuovere una tastiera selezionata.

Nell'area di notifica, cliccando con il tasto sinistro del mouse sull'indicatore della tastiera, è ora possibile selezionare la tastiera tedesca.

.. figure:: images/kbd-layout/tastiera-6.png
    :width: 219px
    :align: center
	    
Dopo la selezione l'indicatore mostrerà :guilabel:`DE`.

.. figure:: images/kbd-layout/tastiera-7.png
    :width: 91px
    :align: center
