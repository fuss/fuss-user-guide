**********
Il desktop
**********

All’inizio  il desktop che si presenta all’utente è il seguente:

.. figure:: images/desktop/desktop.png
    :width: 300px
    :align: center

Descrizione
===========

Il desktop visualizza tre cartelle: 

* Casa, che è la home dell’utente

* File system, che permette la navigazione e la visualizzazione delle cartelle del PC e delle cartelle condivise

* Cestino, per l’eliminazione dei file

Il pannello in basso
====================

In basso c’è un pannello.
 
Sulla sinistra si vede ciò che c’è nella seguente immagine, che possiamo chiamare pannello delle applicazioni principali:

.. figure:: images/desktop/pann_sx.png
    :width: 200px
    :align: center

A destra in basso si trova questo pannello:

Si trovano le seguenti icone, da destra a sinistra:

* data e ora

* gestore dell’audio: nello stato dell’immagine l’audio è disattivato. Per attivarlo cliccare sull’icona con tasto dx del mouse e deselezionare Mute. Poi cliccare col tasto sx sull’icona, compare la barra del volume e trascinare per adeguare il volume stesso

* il selettore della lingua applicata alla tastiera: cliccando si può selezionare tra italiano, inglese e tedesco

* il gestore delle connessioni


.. figure:: images/desktop/pann_dx.png
    :width: 200px
    :align: center

La finestra delle applicazioni
===================================

Cliccando sull’icona di tutte le applicazioni si apre la seguente finestra:

.. figure:: images/desktop/applicazioni.png
    :width: 200px
    :align: center

La finestra delle applicazioni è strutturata in questo modo:

* in alto c’è un pannello che riporta da destra a sinistra: da sinistra a destra:

    * il nome dell’utente

    * un’icona per gestire tutte le impostazioni

    * un’icona per bloccare lo schermo

    * la commutazione tra utenti
    
    * l’uscita dal sistema

* una barra di ricerca ed avvio dei programmi

* due colonne di cui:

    * quella sinistra mostra i programmi presenti solo inizialmente nel pannello dei programmi principali
    
    * quella destra mostra tutte le categorie di programmi, in modo da facilitarne la ricerca








