
Configurazione wifi
-------------------------------
Per potersi connettere alle reti wifi cosiddette **guest** delle scuole ed accedere ad internet mediante un browser (ad esempio firefox, chomium, ...) o 
installare applicazioni, i parametri di configurazione da inserire sui dispositivi sono i seguenti: 

- Tipo di autenticazione: **PEAP**

- Tipo di autenticazione interna: **MSCHAPv2**

- Certificato: al momento non è possibile scegliere alcun certificato

- Credenziali utente di rete con permessi di accesso a ``wifi`` ed ``internet``

In qualche caso i parametri vengono rilevati automaticamente e non vengono richiesti.

Di seguito alcuni esempi:

- Laptop con sistema FUSS 12:

.. image:: images/config-wifi-fuss.png
   :scale: 50%
   :align: center

........................................................................................................................................................................  

- Smartphone con sistema operativo Android aggiornato:

.. image:: images/config-wifi-android.png
   :scale: 20%
   :align: center

........................................................................................................................................................................

- Smartphone con sistema operativo Android datato:

.. image:: images/config-wifi-android-old.jpg
   :scale: 30%
   :align: center

........................................................................................................................................................................
   
Configurazione proxy
---------------------------------
Se, come dovrebbe essere, la connessione internet del dispositico è filtrata da un proxy,
è necessaria la configurazione dello stesso, che differisce da un sistema all’altro.

Dispositivi FUSS 12
**************************
Nei dispositivi fuss solo firefox consente una configurazione del proxy attraverso le ``Impostazioni`` dell’applicazione. 
E’ stato quindi predisposto un semplice applicativo che consente di automatizzare l’attivazione (o la disattivazioni) delle configurazioni.
È sufficiente avviare l’applicazione **fuss-proxy** dal menu in basso a sinistra:

.. image:: images/Menu.png
   :scale: 70%
   :align: center

........................................................................................................................................................................
   
Per accedere alle impostazioni, è necessario avere privilegi di amministrazione sudoer ed  inserire la propria password.

.. image:: images/Autenticazione.png
   :scale: 60%
   :align: center

........................................................................................................................................................................
   
Se permessi e credenziali sono corretti, si apre la finestra principale.

.. image:: images/Gestione_proxy.png
   :scale: 70%
   :align: center

........................................................................................................................................................................
   
A questo punto è sufficiente cliccare sul pulsante **Configura Proxy**, e verranno eseguite automaticamente tutte le modifiche necessarie. 
Per renderle effettive si deve rifare il login alla sessione utente.
Se la configurazione è andata a buon fine si otterrà il messaggio:

.. image:: images/Configurazione_successo.png
   :scale: 50%
   :align: center
   
........................................................................................................................................................................
   
Viceversa, se in seguito si intende utilizzare il dispositivo al di fuori di una rete scolastica, 
si riavvia il programma e si sceglie **Rimuovi proxy**. In caso di successo:

.. image:: images/Rimozione_successo.png
   :scale: 50%
   :align: center
   
........................................................................................................................................................................


Dispositivi Windows
***********************
Nei dispositivi Microsoft la configurazione del proxy può essere eseguita in tre modi:

1.  Rilevamento **automatico** della configurazione 

.. image:: images/Configurazione_automatica.png
   :scale: 30%
   :align: center

........................................................................................................................................................................
   
2. Accesso al file di configurazione **wpad.dat** sul server

.. image:: images/File_configurazione.png
   :scale: 30%
   :align: center

........................................................................................................................................................................

3. Configurazione **manuale** proxy

.. image:: images/Configurazione_manuale.png
   :scale: 30%
   :align: center
   
........................................................................................................................................................................

Dispositivi Android
***********************
Nei dispositivi Android sono possibili due modalità di configurazione:

1. Modalità **automatica**, specificando l` **Indirizzo Web** del file di configurazione

.. image:: images/Proxy_automatico_android.png
   :scale: 30%
   :align: center
 
........................................................................................................................................................................

2. Modalità **manuale**, indicando

- Nome host proxy: ``proxy``

- Porta: ``8080``

.. image:: images/Proxy_manuale_android.png
   :scale: 30%
   :align: center

