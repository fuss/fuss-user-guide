*********************
Dispositivi Bluetooth
*********************

In questo paragrafo viene trattata la configurazione e l'utilizzo di periferiche bluetooth esterne (p.es. casse amplificate, auricolari bluetooth, ecc.) con PC FUSS dotati di dispositivo bluetooth.

Permessi per utilizzare dispositivi bluetooth
=============================================

Il prerequisito affinché un utente possa utilizzare un dispositivo bluetooth è che disponga dei due permessi ``bluetooth`` e ``netdev`` e sulla macchina sia installato il pacchetto ``blueman``. Se non si è amministratori della macchina, rivolgersi all'amministratore di sistema per associare le utenze previste ai due suddetti gruppi (lato server se il PC è un client all'interno della rete scolastica) e per far installare il pacchetto.

Configurazione di casse amplificate o auricolari bluetooth
==========================================================

Se i prerequisiti elencati sono soddisfatti, dopo il login nell'area di notifica in basso a destra si troverà l'icona del ``blueman-applet``

.. figure:: images/bluetooth/bluetooth-01.png
    :width: 298px
    :align: center

Verificare che il dispositivo sia acceso e pronto per la connessione. Cliccando sull'icona del ``blueman-applet`` si apre un menu. Si scelga la voce  :guilabel:`Imposta nuovo dispositivo ...`	    

.. figure:: images/bluetooth/bluetooth-02.png
    :width: 628px
    :align: center

Si avvierà l'assistente per l'installazione di dispositivi bluetooth; cliccare :guilabel:`Successivo`; 
	    
.. figure:: images/bluetooth/bluetooth-03.png
    :width: 755px
    :align: center

Verranno elencati i device disponibili; si selezioni quello cui ci si vuole connettere premendo poi :guilabel:`Successivo`.
	    
.. figure:: images/bluetooth/bluetooth-04.png
    :width: 755px
    :align: center

Nel menu :guilabel:`Connect` si scelga la modalità :guilabel:`Sincronizzazione audio` seguito da :guilabel:`Successivo` 
	    
.. figure:: images/bluetooth/bluetooth-05.png
    :width: 755px
    :align: center

Verà fatto un tentativo di accoppiamento al dispositivo.

.. figure:: images/bluetooth/bluetooth-06.png
    :width: 755px
    :align: center

Se l'associazione si conclude positivmente, la configurazione è terminata e si può premere il tasto :guilabel:`Chiudi`.	    
	    
.. figure:: images/bluetooth/bluetooth-07.png
    :width: 891px
    :align: center

Connessione al dispositivo configurato
======================================
	    
Ogni volta che ci si vorrà connettere al dispositivo, basterà semplicemente selezionare nel menu di ``blueman-applet`` la voce :guilabel:`Connessioni recenti...` scegliendo il dispositivo ed attendendo la conferma di avvenuta connessione.
	    
.. figure:: images/bluetooth/bluetooth-08.png
    :width: 628px
    :align: center

Nel mixer audio in basso a destra resterà solo da scegliere il dispositivo bluetooth per l'uscita audio (e se necessario, nel caso di auricolari bluetooth, anche per il microfono).
	    
.. figure:: images/bluetooth/bluetooth-09.png
    :width: 679px
    :align: center	    
